#include "syscall.h"
#include "htif.h"
#include "byteorder.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <stdlib.h>
#include <assert.h>
#include <termios.h>
#include <sstream>
#include <iostream>
using namespace std::placeholders;

#define RISCV_AT_FDCWD -100

struct riscv_stat
{
  target_endian<uint64_t> dev;
  target_endian<uint64_t> ino;
  target_endian<uint32_t> mode;
  target_endian<uint32_t> nlink;
  target_endian<uint32_t> uid;
  target_endian<uint32_t> gid;
  target_endian<uint64_t> rdev;
  target_endian<uint64_t> __pad1;
  target_endian<uint64_t> size;
  target_endian<uint32_t> blksize;
  target_endian<uint32_t> __pad2;
  target_endian<uint64_t> blocks;
  target_endian<uint64_t> atime;
  target_endian<uint64_t> __pad3;
  target_endian<uint64_t> mtime;
  target_endian<uint64_t> __pad4;
  target_endian<uint64_t> ctime;
  target_endian<uint64_t> __pad5;
  target_endian<uint32_t> __unused4;
  target_endian<uint32_t> __unused5;

  riscv_stat(const struct stat& s, htif_t* htif)
    : dev(htif->to_target<uint64_t>(s.st_dev)),
      ino(htif->to_target<uint64_t>(s.st_ino)),
      mode(htif->to_target<uint32_t>(s.st_mode)),
      nlink(htif->to_target<uint32_t>(s.st_nlink)),
      uid(htif->to_target<uint32_t>(s.st_uid)),
      gid(htif->to_target<uint32_t>(s.st_gid)),
      rdev(htif->to_target<uint64_t>(s.st_rdev)), __pad1(),
      size(htif->to_target<uint64_t>(s.st_size)),
      blksize(htif->to_target<uint32_t>(s.st_blksize)), __pad2(),
      blocks(htif->to_target<uint64_t>(s.st_blocks)),
      atime(htif->to_target<uint64_t>(s.st_atime)), __pad3(),
      mtime(htif->to_target<uint64_t>(s.st_mtime)), __pad4(),
      ctime(htif->to_target<uint64_t>(s.st_ctime)), __pad5(),
      __unused4(), __unused5() {}
};

std::string syscall_t::undo_chroot(const char* fn)
{
  if (chroot.empty())
    return fn;
  if (strncmp(fn, chroot.c_str(), chroot.size()) == 0
      && (chroot.back() == '/' || fn[chroot.size()] == '/'))
    return fn + chroot.size() - (chroot.back() == '/');
  return "/";
}


static void request(struct sk_buff *skb) {
    msg_size=strlen(msg);

    netlink_holder=(struct nlmsghdr*)skb->data;
    printk(KERN_INFO "Netlink received msg payload:%s\n",(char*)nlmsg_data(netlink_holder));

    pid = netlink_holder->nlmsg_pid;                               

    skb_out = nlmsg_new(msg_size,0);

    if(!skb_out){
        printk(KERN_ERR "Failed to allocate new skb\n");
        return;
    } 

    strncpy(nlmsg_data(netlink_holder),msg,msg_size);             
    if(result<0)
        printk(KERN_INFO "Error while sending bak to user\n");
}

reg_t syscall_t::sys_pwrite(reg_t fd, reg_t pbuf, reg_t len, reg_t off, reg_t a4, reg_t a5, reg_t a6)
{
  std::vector<char> buf(len);
  memif->read(pbuf, len, buf.data());
  reg_t ret = sysret_errno(pwrite(fds.lookup(fd), buf.data(), len, off));
  return ret;
}
